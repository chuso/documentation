# Comment on an event

## Add a comment

When comments are allowed for an event, you can add one by using the comments section:

![comments section](../../images/en/rose-comment-event-EN.png)

You also can reply to a specific comment by clicking the **Reply** link:

![reply comments section](../../images/en/comments-replies-event.png)

## Report a comment

!!! note
    You have to be connected to your account to report.

You can report a comment by:

  1. clicking <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M13 14H11V9H13M13 18H11V16H13M1 21H23L12 2L1 21Z" />
    </svg> (available by hovering over the comment):
    ![report comment icon](../../images/en/report-comment-icon.png)
  * [Optional but **recommended**] filling the report modal with details:
    ![comment report modal](../../images/en/comment-report.png)

## Delete your comment

You can delete your own comment by clicking the <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" /></svg> icon (available by hovering over the comment):

![image delete a comment icon](../../images/en/delete-comment.png)
