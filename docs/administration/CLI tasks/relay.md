# Relay

Manages remote relays

## Make your instance follow a mobilizon instance

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.follow <relay_host>
    ```

    Example: 
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.follow example.org
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.follow <relay_host>
    ```

    Example: 
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.follow example.org
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.relay.follow <relay_host>
    ```

    Example: 
    ```bash
    MIX_ENV=prod mix mobilizon.relay.follow example.org
    ```

## Make your instance unfollow a mobilizon instance

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.unfollow <relay_host>
    ```

    Example: 
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.unfollow example.org
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.unfollow <relay_host>
    ```

    Example: 
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.unfollow example.org
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.relay.unfollow <relay_host>
    ```

    Example: 
    ```bash
    MIX_ENV=prod mix mobilizon.relay.unfollow example.org
    ```

## Make your instance refresh a mobilizon instance

It will crawl public events from the remote instance public outbox.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.refresh <relay_host>
    ```

    Example: 
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl relay.refresh example.org
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.refresh <relay_host>
    ```

    Example: 
    ```bash
    docker-compose exec mobilizon mobilizon_ctl relay.refresh example.org
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.relay.refresh <relay_host>
    ```

    Example: 
    ```bash
    MIX_ENV=prod mix mobilizon.relay.refresh example.org
    ```
