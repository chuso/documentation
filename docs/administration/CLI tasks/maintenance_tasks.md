# Maintenance tasks

## Installation

Generates new configuration files.

!!! warning
    This command generates configuration for your Mobilizon instance and should be run only once when installing. 

If any options are left unspecified, you will be prompted interactively.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl instance gen [<options>]
    ```

=== "Docker"
    You shouldn't need to run this task on Docker.

=== "Source"
    ```bash
    mix mobilizon.instance gen [<options>]
    ```
### Options
* `-f`, `--force` Whether to erase existing files
* `-o`, `--output PATH` The path to output the config file. Defaults to `config/runtime.exs`.
* `--output-psql PATH` The path to output the SQL script. Defaults to `setup_db.psql`.
* `--domain DOMAIN` The instance's domain
* `--instance-name INSTANCE_NAME` The instance's name
* `--admin-email ADMIN_EMAIL` The administrator's email
* `--dbhost HOSTNAME` The database hostname of the PostgreSQL database to use
* `--dbname DBNAME` The name of the database to use 
* `--dbuser DBUSER` The database user (aka role) to use for the database connection 
* `--dbpass DBPASS` The database user's password to use for the database connection 
* `--dbport DBPORT` The database port
* `--listen-port PORT` The port the app should listen to, defaults to 4000

## Fix unattached media

*Introduced in Mobilizon 1.0.3*

Fixes an issue with media files that were not attached to their entities when included directly in body text, meaning they would have been swept away when deleting orphan media files.
**This task should only be run once**, and will be removed in version 1.2.0 of Mobilizon.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl maintenance.fix_unattached_media_in_body
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl maintenance.fix_unattached_media_in_body
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.maintenance.fix_unattached_media_in_body
    ```
