---
title: FAQ
---

# FAQ

## Should I have a big server to run Mobilizon?


Not really. Being written in Elixir, Mobilizon doesn't need much resources once it's running. If you plan to open your instance to the public, plan in advance higher values for the following given requirements.

!!! note
    If you plan to self-host a address/geocoding server as well, [the requirements are quite on another level](./configure/geocoders.md).

<dl>
    <dt>CPU</dt>
    <dd><b>One should be enough</b>
    <p>Depending on your number of users and instances you federate with, extra CPUs will be helpful.</p>
    </dd>

    <dt>RAM</dt>
    <dd>
        <b>512MB should be enough for Mobilizon, Nginx and PostgreSQL</b>
        <p>Mobilizon will use at least around ~256MB and PostgreSQL and nginx can use ~20MB. Extra memory can improve tasks like compiling and building dependencies.</p>
    </dd>

    <dt>Storage</dt>
    <dd><b>Depends how many users and events you have</b>
        <p>A little space will be needed for Mobilizon and it's dependencies (damn you <code>node_modules</code>) themselves. Otherwise, storage usage will grow mostly with user's profile pics and pictures associated to events. Also the PostgreSQL database can start to weigh a bit after a while, depending on how many events you create and how many other instances you follow.</p>
    </dd>

    <dt>Bandwidth</dt>
    <dd>Any bandwidth will do, but higher numbers will improve the experience for users and will help federation.</dd>
</dl>

## A Mobilizon dependency fails to compile (source install)

Some Mobilizon dependencies require system tools or libraries to compile (such as `cmake` or `gcc`). You might be missing a system dependency.

Check the [source install guide](./install/source.md) (especially the "Basic tools" section), make sure everything is installed and clean and recompile Mobilizon's dependencies with:

```
MIX_ENV=prod mix deps.clean --all
MIX_ENV=prod mix clean
MIX_ENV=prod mix compile
```

## I am unable to send any emails

The default Mobilizon configuration assumes a local SMTP server is available on the same server. To tweak this for your own setup, [see this page](./configure/email.md).

## What should I backup?

You'll need to backup the PostgreSQL database (we recommend [Barman](https://www.pgbarman.org/)), your config file (such as `/etc/mobilizon/config.exs` or `config/runtime.exs`, depending on how you installed Mobilizon) and the user's picture directory in `uploads/`.

## How can I tweak the instance theme, the logo,…

For now this isn't supported (unless you edit the front-end files directly).

## How can I give a user moderator/administrator permissions?

For now this is [only possible through CLI](CLI tasks/manage_users.md#modify-an-user).

## I can't install dependencies with Yarn
If you have the following error:
```
ERROR: [Errno 2] No such file or directory: 'install'
```
It means that you installed the `yarn` from Debian/Ubuntu directly, which provides the `cmdtest` package, which is completely unrelated. Follow the instructions on the [Source install guide](./install/source.md#yarn) to install the proper tool.

## How can I increase the allowed upload size? Failed to upload media!

There are two possibilities that limit upload size: Your webserver (e.g. nginx) or the Mobilizon configuration.

**Adjust the mobilizon configuration**

You will have to adjust the configuration file. Check out [this page on configuration](/administration/configure/) on how to do this in general.
Then you can adjust the last three lines that are shown in this example to your needs.

```
config :mobilizon, :instance,
  name: System.get_env("MOBILIZON_INSTANCE_NAME", "Mobilizon"),
  description: "Change this to a proper description of your instance",
  hostname: System.get_env("MOBILIZON_INSTANCE_HOST", "mobilizon.lan"),
  registrations_open: System.get_env("MOBILIZON_INSTANCE_REGISTRATIONS_OPEN", "false") == "true",
  demo: false,
  allow_relay: true,
  federating: true,
  upload_limit: 10_485_760, #ChangeMe
  avatar_upload_limit: 10_097_152, #ChangeMe 
  banner_upload_limit: 10_194_304, #ChangeMe
```





**Adjust your webserver**

Nginx usually limits file 1 mb. You can increase this by setting `client_max_body_size 16m;` in the server block. This should also be possible for other webservers.

## How to upgrade PostgreSQL ?

!!! info
    If you're using an install based on Docker, it is *required* to dump and restore when upgrading the `postgis` image to new major versions of PostgreSQL or PostGIS. A version of `postgresql-client` is included in the `mobilizon` image if needed.

We use the PostGIS extension to hold the geospatial data. Migrating to a new major PostgreSQL version (with `pg_upgradecluster`) is not supported when there's also a PostGIS upgrade.

For instance migrating from Debian Buster (10) to Bullseye (11) upgrades PostgreSQL from 11 to 13, but also Postgis from 2.5 to 3.1, so you won't be able to perform a simple upgrade.

In this case, it's better to:

* Dump the database (with `pg_dump`) from the old PostgreSQL cluster.
* Stop the old cluster.
* Create and start the new one (if not already done when upgrading the system).
* Create the database, database user back (according to the credentials in your mobilizon configuration).
* Add the extensions back to the database `CREATE EXTENSION extension` (where extension is `postgis`, `pg_trgm` and `unaccent`).
* Import the dump to your new cluster (with `pg_restore`).

!!! warning
    If you have other databases that don't use Postgis on the same server, you need to `DROP` the mobilizon database (or any other database that uses Postgis and that you have already dumped), then perform the `pg_upgradecluster` command to migrate these simple databases to the new cluster.

If everything works fine after the upgrade and after making sure you are running on the new cluster, you may drop the old cluster and remove the `postgresql-11` dependencies.

A few links related to this:

* Full tutorial (for 9.6 to 11 upgrade, don't forget to adapt) https://rigacci.org/wiki/doku.php/doc/appunti/linux/sa/postgresql_upgrade
* Debian documentation https://wiki.debian.org/DebianGis/UpdatingPostGIS
* https://www.bostongis.com/blog/index.php?/archives/268-Using-pg_upgrade-to-upgrade-PostGIS-without-installing-an-older-version-of-PostGIS.html

In french 🇫🇷:

* https://wiki.fiat-tux.fr/books/administration-syst%C3%A8mes/page/migration-d%E2%80%99une-version-majeure-de-postgresql-%C3%A0-une-autre
* https://oslandia.com/2020/11/05/mettre-a-jour-vos-vieux-clusters-postgis/
